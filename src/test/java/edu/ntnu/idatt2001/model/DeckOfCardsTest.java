package edu.ntnu.idatt2001.model;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

@Nested
public class DeckOfCardsTest {

    @Test
    void dealHandTest() {

        DeckOfCards deck = new DeckOfCards();
        int startSize = deck.getCardsLeft();
        deck.dealHand(5);

        assertTrue(startSize - 5 == deck.getCardsLeft());

    }
}
