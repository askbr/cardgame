package edu.ntnu.idatt2001.controller;

import edu.ntnu.idatt2001.model.DeckOfCards;
import edu.ntnu.idatt2001.model.HandOfCards;
import edu.ntnu.idatt2001.model.PlayingCard;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class GameController implements Initializable {
    //FXML variables
    @FXML private ImageView card1;
    @FXML private ImageView card2;
    @FXML private ImageView card3;
    @FXML private ImageView card4;
    @FXML private ImageView card5;
    @FXML private Label title;

    //Other Variables
    private DeckOfCards deckOfCards;
    private HandOfCards currentHand;

    //Constants
    private final static int HAND_SIZE = 5;

    @FXML
    void checkHand(ActionEvent event) throws IOException {

        URL url = new File("src/main/resources/edu/ntnu/idatt2001/view/CheckHand.fxml").toURI().toURL();
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        CheckHandController checkHandController = fxmlLoader.getController();
        checkHandController.initValues(currentHand);
        stage.show();
    }

    @FXML
    void dealHand(ActionEvent event) {
        if (deckOfCards.getCardsLeft() < HAND_SIZE) deckOfCards = new DeckOfCards();
        HandOfCards handOfCards = deckOfCards.dealHand(HAND_SIZE);
        currentHand = handOfCards;

        List<PlayingCard> handList = handOfCards.getHand();

        card1.setImage(getCardImage(handList.get(0).toString()));
        card2.setImage(getCardImage(handList.get(1).toString()));
        card3.setImage(getCardImage(handList.get(2).toString()));
        card4.setImage(getCardImage(handList.get(3).toString()));
        card5.setImage(getCardImage(handList.get(4).toString()));

    }

    @FXML
    void findFlush(MouseEvent event) {
        ArrayList<PlayingCard> flush = new ArrayList<>();
        flush.add(new PlayingCard('S', 10));
        flush.add(new PlayingCard('S', 11));
        flush.add(new PlayingCard('S', 12));
        flush.add(new PlayingCard('S', 13));
        flush.add(new PlayingCard('S', 1));
        currentHand = new HandOfCards(flush);

        card1.setImage(getCardImage(flush.get(0).toString()));
        card2.setImage(getCardImage(flush.get(1).toString()));
        card3.setImage(getCardImage(flush.get(2).toString()));
        card4.setImage(getCardImage(flush.get(3).toString()));
        card5.setImage(getCardImage(flush.get(4).toString()));
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        deckOfCards = new DeckOfCards();
    }

    private Image getCardImage(String card) {
        return new Image("/edu/ntnu/idatt2001/images/cards/" + card + ".png");
    }
}
