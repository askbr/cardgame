package edu.ntnu.idatt2001.controller;

import edu.ntnu.idatt2001.model.HandOfCards;
import edu.ntnu.idatt2001.model.PlayingCard;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class CheckHandController {

    @FXML
    private TextField sumOfCards;

    @FXML
    private TextField heartCards;

    @FXML
    private TextField spadeQueen;

    @FXML
    private TextField flush;


    public void initValues(HandOfCards handOfCards) {
        this.sumOfCards.setText(String.valueOf(handOfCards.calculateHandSum()));
        this.heartCards.setText(handOfCards.playingCardListToString(handOfCards.getCardsOfHearts()));
        this.spadeQueen.setText((handOfCards.containsCard(new PlayingCard('S',12))) ? "In hand" : "Not In Hand");
        this.flush.setText((handOfCards.isFlush()) ? "Flush!" : "Not flush");

    }

}