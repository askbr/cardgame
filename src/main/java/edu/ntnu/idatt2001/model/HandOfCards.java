package edu.ntnu.idatt2001.model;

import java.util.List;
import java.util.stream.Collectors;

public class HandOfCards {
    private List<PlayingCard> hand;

    public HandOfCards(List<PlayingCard> handOfCards) {
        this.hand = handOfCards;
    }

    public List<PlayingCard> getHand() {
        return hand;
    }

    public boolean isFlush() {
        boolean isFlush = true;

        for (int i = 1; i<hand.size(); i++) {
            if (hand.get(i-1).getSuit() != hand.get(i).getSuit()) {
                isFlush = false;
            }
        }
        return isFlush;
    }

    public boolean containsCard(PlayingCard card) {
        for (PlayingCard p : hand) {
            if (p.equals(card)) return true;
        }
        return false;
    }

    public int calculateHandSum() {
        return hand.stream().map(PlayingCard::getFace).reduce((a,b) -> a+b).get();
    }

    public List<PlayingCard> getCardsOfHearts() {
        return hand.stream().filter(c -> c.getSuit() == 'H').collect(Collectors.toList());
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();

        for (PlayingCard p : hand) {
            ret.append(p + ", ");
        }

        return ret.toString();
    }

    public String playingCardListToString(List<PlayingCard> playingCardList) {
        String str = "";
        for (PlayingCard card : playingCardList) {
            str += card.toString() + " ";
        }
        return str;
    }
}
