package edu.ntnu.idatt2001.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DeckOfCards {
    //Constants
    private final char[] suit = { 'S', 'H', 'D', 'C' };

    private List<PlayingCard> cards;

    public DeckOfCards() {
        cards = deckGenerator();

    }

    public HandOfCards dealHand(int n){
        Random r = new Random();
        ArrayList<PlayingCard> hand = new ArrayList<>();
        for (int i = 0; i<n; i++) {
            int randomCard = r.nextInt(cards.size());
            hand.add(cards.get(randomCard));
            cards.remove(randomCard);
        }
        return new HandOfCards(hand);
    }

    private List<PlayingCard> deckGenerator() {
        ArrayList<PlayingCard> deck = new ArrayList<>();

        for (int i = 0; i<4; i++) {
            for (int j = 1; j<=13; j++) {
                PlayingCard card = new PlayingCard(suit[i], j);
                deck.add(card);
            }
        }
        return deck;
    }

    public int getCardsLeft() {
        return cards.size();
    }
}