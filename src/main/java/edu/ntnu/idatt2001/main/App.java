package edu.ntnu.idatt2001.main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        Scene scene = new Scene(loadFXML("Game"));
        stage.setScene(scene);
        stage.setTitle("Poker Game (V0.1)");
        stage.show();

    }

    private static Parent loadFXML(String fxml) throws IOException {
        URL url = new File("src/main/resources/edu/ntnu/idatt2001/view/" + fxml + ".fxml").toURI().toURL();
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}