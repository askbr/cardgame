module edu.ntnu.idatt2001 {
    requires javafx.controls;
    requires javafx.fxml;

    opens edu.ntnu.idatt2001.controller to javafx.fxml;
    exports edu.ntnu.idatt2001.controller;

    opens edu.ntnu.idatt2001.main to javafx.fxml;
    exports edu.ntnu.idatt2001.main;
}